﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;

namespace DSALGONetFr
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //NameTest nameTest = new NameTest();

            //NameTest.CallName();

            IntStruct.Main1();

            Console.ReadKey();

            // davayte
        }
    }

    public struct Name
    {
        private string fname, lname, mname;

        public Name(string fname, string lname, string mname)
        {
            this.fname = fname;
            this.lname = lname;
            this.mname = mname;
        }

        public string firstName
        {
            get { return fname; }
            set { fname = value; } // firstName
        }

        public string middleName
        {
            get { return mname; }
            set { mname = value; }
        }

        public string lastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public override string ToString()
        {
            return (String.Format("{0} {1} {2}", fname, mname, lname));
        }

        public string Initials()
        {
            return (String.Format("{0} {1} {2}", fname.Substring(0, 1), mname.Substring(0, 1), lname.Substring(0, 1)));
        }

    }

    public class NameTest
    {
        public static void CallName()
        {
            Name myName = new Name("Michael", "Jason", "Macmillan");
            string fullName, inits;

            fullName = myName.ToString();
            inits = myName.Initials();

            Console.WriteLine("My name is: {0}", fullName);
            Console.WriteLine("My initials are: {0}", inits);
        }
    }

    public class IntStruct
    {
        public static void Main1()
        {
            int num;
            string snum;

            Console.WriteLine("Enter a number: ");
            snum = Console.ReadLine();

            num = int.Parse(snum);

            Console.WriteLine(num);
        }
    }

}
